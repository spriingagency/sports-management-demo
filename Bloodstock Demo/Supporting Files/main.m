//
//  main.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 04/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
