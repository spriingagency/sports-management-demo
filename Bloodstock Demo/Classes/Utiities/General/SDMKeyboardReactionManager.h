//
//  SDMKeyboardReactionManager.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef void (^SDMKeyboardVisibilityChangedBlock)(NSDictionary *userInfo);

@interface SDMKeyboardReactionManager : NSObject

-(void) setKeyBoardWillShowHandler:(SDMKeyboardVisibilityChangedBlock)handler;
-(void) setKeyBoardDidShowHandler:(SDMKeyboardVisibilityChangedBlock)handler;
-(void) setKeyBoardWillHideHandler:(SDMKeyboardVisibilityChangedBlock)handler;
-(void) setKeyBoardDidHideHandler:(SDMKeyboardVisibilityChangedBlock)handler;

@end
