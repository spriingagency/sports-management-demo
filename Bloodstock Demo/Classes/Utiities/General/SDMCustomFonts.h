//
//  SDMCustomFonts.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 11/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDMCustomFonts : NSObject

#pragma mark - Bebas Neue
-(NSString*) bebasNeueBookFontName;
-(NSString*) bebasNeueLightFontName;
-(NSString*) bebasNeueBoldFontName;
-(NSString*) bebasNeueThinFontName;
-(NSString*) bebasNeueRegularFontName;

@end
