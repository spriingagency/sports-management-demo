//
//  SDMKeyboardReactionManager.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMKeyboardReactionManager.h"

@interface SDMKeyboardReactionManager ()

@property (nonatomic, copy) SDMKeyboardVisibilityChangedBlock keyboardWillShowBlock;
@property (nonatomic, copy) SDMKeyboardVisibilityChangedBlock keyboardDidShowBlock;
@property (nonatomic, copy) SDMKeyboardVisibilityChangedBlock keyboardWillHideBlock;
@property (nonatomic, copy) SDMKeyboardVisibilityChangedBlock keyboardDidHideBlock;

@end

@implementation SDMKeyboardReactionManager

#pragma mark - Public
-(void) setKeyBoardWillShowHandler:(SDMKeyboardVisibilityChangedBlock)handler
{
    self.keyboardWillShowBlock = handler;
}

-(void) setKeyBoardDidShowHandler:(SDMKeyboardVisibilityChangedBlock)handler
{
    self.keyboardDidShowBlock = handler;
}

-(void) setKeyBoardWillHideHandler:(SDMKeyboardVisibilityChangedBlock)handler
{
    self.keyboardWillHideBlock = handler;
}

-(void) setKeyBoardDidHideHandler:(SDMKeyboardVisibilityChangedBlock)handler
{
    self.keyboardDidHideBlock = handler;
}

#pragma mark - Init & Dealloc
-(instancetype) init
{
    self = [super init];
    if (!self) return nil;
    [self registerForKeyboardNotifications];
    return self;
}

-(void) awakeFromNib
{
    [self registerForKeyboardNotifications];
}

-(void) dealloc
{
    [[NSNotificationCenter defaultCenter] removeObserver:self];
}

-(void) registerForKeyboardNotifications
{
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillShow:) name:@"UIKeyboardWillShowNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidShow:) name:@"UIKeyboardDidShowNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardWillHide:) name:@"UIKeyboardWillHideNotification" object:nil];
    [[NSNotificationCenter defaultCenter] addObserver:self selector:@selector(keyboardDidHide:) name:@"UIKeyboardDidHideNotification" object:nil];
}

#pragma mark - React to Notifications
-(void) keyboardWillShow:(NSNotification*)notification
{
    if (self.keyboardWillShowBlock) {
        self.keyboardWillShowBlock(notification.userInfo ?: nil);
    }
}

-(void) keyboardDidShow:(NSNotification*)notification
{
    if (self.keyboardDidShowBlock) {
        self.keyboardDidShowBlock(notification.userInfo ?: nil);
    }
}

-(void) keyboardWillHide:(NSNotification*)notification
{
    if (self.keyboardWillHideBlock) {
        self.keyboardWillHideBlock(notification.userInfo ?: nil);
    }
}

-(void) keyboardDidHide:(NSNotification*)notification
{
    if (self.keyboardDidHideBlock) {
        self.keyboardDidHideBlock(notification.userInfo ?: nil);
    }
}

@end
