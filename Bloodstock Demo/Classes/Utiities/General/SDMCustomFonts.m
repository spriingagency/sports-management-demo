//
//  SDMCustomFonts.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 11/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMCustomFonts.h"

@implementation SDMCustomFonts

#pragma mark - Bebas Neue
-(NSString*) bebasNeueBookFontName
{
    return @"BebasNeueBook";
}

-(NSString*) bebasNeueLightFontName
{
    return @"BebasNeueLight";
}

-(NSString*) bebasNeueBoldFontName
{
    return @"BebasNeueBold";
}

-(NSString*) bebasNeueThinFontName
{
    return @"BebasNeue-Thin";
}

-(NSString*) bebasNeueRegularFontName
{
    return @"BebasNeueRegular";
}

@end
