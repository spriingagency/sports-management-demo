//
//  UINavigationController+OldStyleTransition.h
//  uniquedoc
//
//  Created by Neil Morrison on 11/06/2014.
//  Copyright (c) 2014 Uniquedoc. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UINavigationController (OldStyleTransition) <UINavigationControllerDelegate>

-(void) useOldStyle;

@end
