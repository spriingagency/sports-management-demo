//
//  SDMDismissSegue.m
//  Bloodstock Demo
//
//  Created by Mark Corbyn on 17/03/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMDismissSegue.h"

@implementation SDMDismissSegue

- (void)perform {
    UIViewController *sourceViewController = self.sourceViewController;
    [sourceViewController.presentingViewController dismissViewControllerAnimated:YES completion:nil];
}

@end