//
//  UINavigationController+OldStyleTransition.m
//  uniquedoc
//
//  Created by Neil Morrison on 11/06/2014.
//  Copyright (c) 2014 Uniquedoc. All rights reserved.
//

#import "UINavigationController+OldStyleTransition.h"
#import "OldStyleNavigationControllerAnimatedTransition.h"

@implementation UINavigationController (OldStyleTransition)


-(void) useOldStyle{
    self.delegate = self;
}

-(id<UIViewControllerAnimatedTransitioning>)navigationController:
(UINavigationController *)navigationController animationControllerForOperation:(UINavigationControllerOperation)operation fromViewController:(UIViewController *)fromVC toViewController:(UIViewController *)toVC
{
    OldStyleNavigationControllerAnimatedTransition * animation = [[OldStyleNavigationControllerAnimatedTransition alloc] init];
    animation.operation = operation;
    return animation;
}

@end
