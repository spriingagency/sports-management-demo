//
//  SDMNotesDataStore.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 18/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface SDMNotesDataStore : NSObject

+(NSArray*) notes;

@end
