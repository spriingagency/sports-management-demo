//
//  SDMAddCommentViewController.h
//  Bloodstock Demo
//
//  Created by Mark Corbyn on 17/03/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>

@class SDMAddCommentViewController;

extern NSString * const SDMCustomCommentTextKey;
extern NSString * const SDMCustomCommentDateKey;

@interface SDMAddCommentViewController : UIViewController

@end
