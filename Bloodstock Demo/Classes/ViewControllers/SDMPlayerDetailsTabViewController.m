//
//  SDMPlayerDetailsTabViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 13/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMPlayerDetailsTabViewController.h"

@interface SDMPlayerDetailsTabViewController ()

@end

@implementation SDMPlayerDetailsTabViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    [self.tabBar setHidden:YES];
}

-(void) selectInfoTab
{
    self.selectedIndex = 0;
}
-(void) selectResultsTab
{
    self.selectedIndex = 1;
}
-(void) selectCostsTab
{
    self.selectedIndex = 2;
}


@end
