//
//  SDMPlayerSearchViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 12/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMPlayerSearchViewController.h"
#import "SDMPlayerDataStore.h"

#import "UINavigationController+OldStyleTransition.h"

#import "FSInteractiveMapView.h"
#import "NMRangeSlider.h"

#define kMapIdentifierEurope @"europe"
#define kMapIdentifierNorthAmerica @"north_america"
#define kMapIdentifierSouthAmerica @"south_america"
#define kMapIdentifierAsia @"asia"
#define kMapIdentifierAfrica @"africa"
#define kMapIdentifierAustralia @"australia"


@interface SDMPlayerSearchViewController () <UITableViewDataSource, UITableViewDelegate>

@property (nonatomic, weak) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UITextField *searchBox;
@property (weak, nonatomic) FSInteractiveMapView *mapView;
@property (weak, nonatomic) IBOutlet UIView *mapContainerView;

@property (weak, nonatomic) CAShapeLayer *selectedLayer;

@property (weak, nonatomic) IBOutlet UIButton *maleButton;
@property (weak, nonatomic) IBOutlet UIButton *femaleButton;
@property (weak, nonatomic) IBOutlet UIButton *regionButton;
@property (weak, nonatomic) IBOutlet NMRangeSlider *rangeSlider;
@property (weak, nonatomic) IBOutlet UIButton *ageLabel;

@property (strong, nonatomic) NSArray *players;
@end

@implementation SDMPlayerSearchViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController useOldStyle];
    
    [self.view setBackgroundColor:[UIColor colorWithWhite:0.f alpha:0.f]];
    [self.tableView setContentInset:UIEdgeInsetsMake(10, 0, 0, 0)];
    
    [self customiseSearchBox];
    [self customiseMapControl];
    [self customiseRangeSlider];
    
    self.players = [SDMPlayerDataStore playerNames];
}

- (void) viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    [self.rangeSlider setLowerValue:20];
    [self.rangeSlider setUpperValue:40];
}

#pragma mark - UI Customisation
-(void) customiseSearchBox
{
    [self.searchBox setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:self.searchBox.placeholder
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor colorWithWhite:1.f alpha:0.5f]}]];
    
    UIView *leftView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 20, self.searchBox.frame.size.height)];
    leftView.backgroundColor = self.searchBox.backgroundColor;
    self.searchBox.leftView = leftView;
    self.searchBox.leftViewMode = UITextFieldViewModeAlways;
    
    UIView *rightView = [[UIView alloc] initWithFrame:CGRectMake(0, 0, 30, self.searchBox.frame.size.height)];
    rightView.backgroundColor = self.searchBox.backgroundColor;
    self.searchBox.rightView = rightView;
    self.searchBox.rightViewMode = UITextFieldViewModeAlways;
}

-(void) customiseMapControl
{
    // the map library crashes if all the contients are set to the same value due to how it calculates its colors. We will instead set the color manually later
    NSDictionary* data = @{ kMapIdentifierAsia : @10,
                            kMapIdentifierAustralia : @5,
                            kMapIdentifierNorthAmerica : @3,
                            kMapIdentifierSouthAmerica : @2,
                            kMapIdentifierAfrica : @5,
                            kMapIdentifierEurope : @1
                            };
    
    FSInteractiveMapView* map = [[FSInteractiveMapView alloc] initWithFrame:self.mapContainerView.bounds];
    [map loadMap:@"world-continents-low" withData:data colorAxis:@[[UIColor blackColor], [UIColor redColor]]];
    
    [map enumerateLayersUsingBlock:^(NSString *identifier, CAShapeLayer *layer) {
        // get the selected layer
        if ([identifier isEqualToString:kMapIdentifierEurope]) {
            self.selectedLayer = layer;
            self.selectedLayer.fillColor = [UIColor redColor].CGColor;
        } else {
            layer.fillColor = [UIColor blackColor].CGColor;
        }
        
        // remove stroke
        layer.strokeColor = [UIColor clearColor].CGColor;
    }];
    
    [map setClickHandler:^(NSString* identifier, CAShapeLayer* layer) {
        if (layer == self.selectedLayer) {
            return;
        }
        
        self.selectedLayer.fillColor = [UIColor blackColor].CGColor;
        self.selectedLayer = layer;
        self.selectedLayer.fillColor = [UIColor redColor].CGColor;
        
        NSString *region = [self regionNameForIdentifier:identifier];
        [self.regionButton setTitle:region forState:UIControlStateNormal];
        
    }];
    
    self.mapContainerView.backgroundColor = [UIColor clearColor];
    [self.mapContainerView addSubview:map];
    self.mapView = map;
}

-(void) customiseRangeSlider
{
    self.rangeSlider.minimumValue = 10;
    self.rangeSlider.maximumValue = 60;
    self.rangeSlider.stepValue = 1;
    self.rangeSlider.lowerValue = 20;
    self.rangeSlider.upperValue = 40;
    
    UIImage* image = [UIImage imageNamed:@"slider-default7-track"];
    image = [image resizableImageWithCapInsets:UIEdgeInsetsMake(0.0, 2.0, 0.0, 2.0)];
    image = [image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.rangeSlider.trackImage = image;
}

#pragma mark - Touch Handling
- (IBAction)maleButtonPressed:(UIButton *)sender
{
    [self.femaleButton setSelected:NO];
    [sender setSelected:YES];
}
- (IBAction)femaleButtonPressed:(UIButton *)sender
{
    [self.maleButton setSelected:NO];
    [sender setSelected:YES];
}
- (IBAction)regionButtonPressed:(id)sender {
}
- (IBAction)ageValueChanged:(NMRangeSlider *)sender {
    [self updateAgeLabel];
}

#pragma mark - Region Handling
-(NSString*) regionNameForIdentifier:(NSString*)identifier
{
    NSDictionary *regionNameDictionary = @{ kMapIdentifierAsia : @"Asia",
                                            kMapIdentifierAustralia : @"Australasia",
                                            kMapIdentifierNorthAmerica : @"North America",
                                            kMapIdentifierSouthAmerica : @"South America",
                                            kMapIdentifierAfrica : @"Africa",
                                            kMapIdentifierEurope : @"Europe"
                                            };
    
    return [regionNameDictionary objectForKey:identifier];
}

#pragma mark - Age Handling
-(void) updateAgeLabel
{
    NSString *ageString = [NSString stringWithFormat:@"%d-%d", (int)self.rangeSlider.lowerValue, (int)self.rangeSlider.upperValue];
    [self.ageLabel setTitle:ageString forState:UIControlStateNormal];
}

#pragma mark - UITableViewDataSource
-(NSInteger) numberOfSectionsInTableView:(UITableView *)tableView
{
    return self.players.count;
}

-(NSInteger) tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    NSArray *sectionArray = self.players[section];
    return sectionArray.count;
}

-(UITableViewCell*) tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell;
    if (indexPath.row == 0) {
        cell = [tableView dequeueReusableCellWithIdentifier:@"header" forIndexPath:indexPath];
    } else {
        cell = [tableView dequeueReusableCellWithIdentifier:@"cell" forIndexPath:indexPath];
    }
    
    NSArray *sectionArray = self.players[indexPath.section];
    NSString *value = sectionArray[indexPath.row];
    cell.textLabel.text = value;
    
    return cell;
}

-(void) tableView:(UITableView *)tableView willDisplayCell:(UITableViewCell *)cell forRowAtIndexPath:(NSIndexPath *)indexPath
{
    [cell setBackgroundColor:[UIColor colorWithWhite:0.f alpha:0.f]];
}

-(CGFloat) tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if (indexPath.row == 0) {
        return 75.f;
    }
    
    return 30.f;
}

#pragma mark - UITableViewDelegate
-(void) tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    UITableViewCell *cell = [tableView cellForRowAtIndexPath:indexPath];
    [cell setSelected:NO];
}

#pragma mark - Unwind
-(IBAction) unwindToPlayerSearch:(UIStoryboardSegue*)segue {

}

@end
