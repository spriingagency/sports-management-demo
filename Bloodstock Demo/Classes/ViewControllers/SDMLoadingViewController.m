//
//  SDMLoadingViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 04/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMLoadingViewController.h"
#import "UINavigationController+OldStyleTransition.h"

@interface SDMLoadingViewController ()

@end

@implementation SDMLoadingViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.navigationController useOldStyle];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, (int64_t)(1 * NSEC_PER_SEC)), dispatch_get_main_queue(), ^{
        [self performSegueWithIdentifier:@"moveToLogin" sender:nil];
    });
}


@end
