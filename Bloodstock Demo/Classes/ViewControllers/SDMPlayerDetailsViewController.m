//
//  SDMPlayerDetailsViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 12/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMPlayerDetailsViewController.h"
#import "SDMPlayerDetailsTabViewController.h"

@interface SDMPlayerDetailsViewController ()

@property (weak, nonatomic) SDMPlayerDetailsTabViewController *tabController;

@property (weak, nonatomic) IBOutlet UIButton *infoTabButton;
@property (weak, nonatomic) IBOutlet UIButton *resultsTabButton;
@property (weak, nonatomic) IBOutlet UIButton *costsTabButton;
@end

@implementation SDMPlayerDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *destinationViewController = (UIViewController*)segue.destinationViewController;
    if ([destinationViewController isKindOfClass:[SDMPlayerDetailsTabViewController class]]) {
        self.tabController = (SDMPlayerDetailsTabViewController*)destinationViewController;
    }
}

#pragma mark - Interface Handling
-(IBAction)backbuttonPressed:(id)sender {
    [self.navigationController popViewControllerAnimated:YES];
}

- (IBAction)infoTabButtonPressed:(id)sender {
    [self deselectAllTabButtons];
    [sender setSelected:YES];
    [self.tabController selectInfoTab];
}
- (IBAction)resultsTabButtonPressed:(id)sender {
    [self deselectAllTabButtons];
    [sender setSelected:YES];
    [self.tabController selectResultsTab];
}
- (IBAction)costsTabButtonPressed:(id)sender {
    [self deselectAllTabButtons];
    [sender setSelected:YES];
    [self.tabController selectCostsTab];
}

#pragma mark - TabButtons
-(void) deselectAllTabButtons
{
    [self.infoTabButton setSelected:NO];
    [self.resultsTabButton setSelected:NO];
    [self.costsTabButton setSelected:NO];
}


@end
