//
//  SDMEventDetailsViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMEventDetailsViewController.h"
#import "UINavigationController+OldStyleTransition.h"

@interface SDMEventDetailsViewController ()

@end

@implementation SDMEventDetailsViewController

-(IBAction)playerButtonPressed:(id)sender
{
    [self.navigationController useOldStyle];
    UIViewController *viewcontroller = [self.storyboard instantiateViewControllerWithIdentifier:@"playerDetails"];
    [self.navigationController pushViewController:viewcontroller animated:YES];
}

@end
