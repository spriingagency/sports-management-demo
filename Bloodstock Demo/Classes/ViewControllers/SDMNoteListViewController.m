//
//  SDMNoteListViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 12/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMNoteListViewController.h"
#import "SDMNoteCollectionViewCell.h"
#import "SDMNotesDataStore.h"

@interface SDMNoteListViewController () <UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout>

@property (strong, nonatomic) NSArray* noteList;

@end

@implementation SDMNoteListViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
    
    self.noteList = [SDMNotesDataStore notes];
}


#pragma mark - UICollectionViewDataSource
-(NSInteger) numberOfSectionsInCollectionView:(UICollectionView *)collectionView
{
    return 1;
}

-(NSInteger) collectionView:(UICollectionView *)collectionView numberOfItemsInSection:(NSInteger)section
{
    return self.noteList.count;
}

-(UICollectionViewCell*) collectionView:(UICollectionView *)collectionView cellForItemAtIndexPath:(NSIndexPath *)indexPath
{
    SDMNoteCollectionViewCell *cell = [collectionView dequeueReusableCellWithReuseIdentifier:@"cell" forIndexPath:indexPath];
    NSDictionary *note = self.noteList[indexPath.row];
    cell.dayLabel.text = note[@"day"];
    cell.monthLabel.text = note[@"month"];
    cell.typeLabel.text = note[@"type"];
    cell.personLabel.text = note[@"name"];
    cell.imageView.image = [UIImage imageNamed:note[@"image"]];
    return cell;
}

#pragma mark - UICOllectionViewDelegate

@end
