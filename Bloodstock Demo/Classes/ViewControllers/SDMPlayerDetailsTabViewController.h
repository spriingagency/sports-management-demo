//
//  SDMPlayerDetailsTabViewController.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 13/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDMPlayerDetailsTabViewController : UITabBarController

-(void) selectInfoTab;
-(void) selectResultsTab;
-(void) selectCostsTab;

@end
