//
//  SDMNoteDetailsViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 13/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMNoteDetailsViewController.h"
#import "SDMCommentCell.h"
#import "SDMAddCommentViewController.h"

@interface SDMNoteDetailsViewController () <UITableViewDelegate, UITableViewDataSource>

@property (weak, nonatomic) IBOutlet UITableView *tableView;

@end

@implementation SDMNoteDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void)viewWillAppear:(BOOL)animated {
    [super viewWillAppear:animated];

    [self.tableView reloadData];
}


-(IBAction)closeButtonPressed:(id)sender
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


#pragma mark - UITableView
- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section {

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    if ([defaults objectForKey:SDMCustomCommentTextKey]) {
        return 1;
    }

    return 0;
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath {
    SDMCommentCell *cell = [tableView dequeueReusableCellWithIdentifier:@"Cell"];

    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSString *comment = [defaults objectForKey:SDMCustomCommentTextKey];
    NSString *dateString = [defaults objectForKey:SDMCustomCommentDateKey];

    cell.commentLabel.text = comment;
    cell.dateLabel.text = dateString;

    return cell;
}

@end
