//
//  SDMAddCommentViewController.m
//  Bloodstock Demo
//
//  Created by Mark Corbyn on 17/03/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//


#import "SDMAddCommentViewController.h"

NSString * const SDMCustomCommentTextKey = @"custom_comment_text";
NSString * const SDMCustomCommentDateKey = @"custom_comment_date_string";

@interface SDMAddCommentViewController () <UITextViewDelegate>

@property (weak, nonatomic) IBOutlet UITextView *textView;

@end

@implementation SDMAddCommentViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    [self.textView becomeFirstResponder];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)sendCommentPressed:(id)sender {
    [self storeCommentIfPresent];
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (void)storeCommentIfPresent {
    NSString *comment = self.textView.text;
    if ([comment length] > 0) {
        NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
        [defaults setObject:comment forKey:SDMCustomCommentTextKey];

        NSDateFormatter *formatter = [[NSDateFormatter alloc] init];
        [formatter setDateFormat:@"dd MMM yyyy"];
        NSString *dateString = [formatter stringFromDate:[NSDate date]];
        [defaults setObject:dateString forKey:SDMCustomCommentDateKey];

        [defaults synchronize];
    }
}

#pragma mark - UITextView Delegate


- (BOOL)textView:(UITextView *)textView shouldChangeTextInRange:(NSRange)range replacementText:(NSString *)text {
    if ([text isEqualToString:@"\n"]) {

        [self storeCommentIfPresent];
        [self dismissViewControllerAnimated:YES completion:nil];
        return NO;
    }
    return YES;
}

@end
