//
//  SDMSettingsViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMSettingsViewController.h"

@interface SDMSettingsViewController ()
@property (weak, nonatomic) IBOutlet UISwitch *notificationsGlobalSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *notificationNewNoteSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *notificationNewCommentSwitch;
@property (weak, nonatomic) IBOutlet UISwitch *playerUpdateNotificationSwitch;

@end

@implementation SDMSettingsViewController

-(IBAction) logoutButtonPressed
{
    [self.delegate didPressLogout];
}

- (IBAction)globalNotificationSwitchChanged:(UISwitch *)sender {
    
    if (sender.isOn) {
        [self.notificationNewNoteSwitch setOn:YES animated:YES];
        [self.notificationNewCommentSwitch setOn:YES animated:YES];
        [self.playerUpdateNotificationSwitch setOn:YES animated:YES];
    } else {
        [self.notificationNewNoteSwitch setOn:NO animated:YES];
        [self.notificationNewCommentSwitch setOn:NO animated:YES];
        [self.playerUpdateNotificationSwitch setOn:NO animated:YES];
    }
}
- (IBAction)subSwitchChanged:(UISwitch *)sender {
    if (sender.isOn) {
        [self.notificationsGlobalSwitch setOn:YES animated:YES];
        return;
    }
    
    if (self.notificationNewCommentSwitch.isOn || self.notificationNewNoteSwitch.isOn || self.playerUpdateNotificationSwitch.isOn) {
        return;
    }
    [self.notificationsGlobalSwitch setOn:NO animated:YES];
    
}
@end
