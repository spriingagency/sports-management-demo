//
//  SDMCommentCell.m
//  Bloodstock Demo
//
//  Created by Mark Corbyn on 17/03/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMCommentCell.h"

@implementation SDMCommentCell

- (void)awakeFromNib {
    // Initialization code
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
