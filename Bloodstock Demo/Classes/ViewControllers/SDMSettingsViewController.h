//
//  SDMSettingsViewController.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>

@protocol SDMSettingsViewControllerDelegate <NSObject>

-(void) didPressLogout;

@end

@interface SDMSettingsViewController : UIViewController

@property (nonatomic, weak) id<SDMSettingsViewControllerDelegate> delegate;

@end
