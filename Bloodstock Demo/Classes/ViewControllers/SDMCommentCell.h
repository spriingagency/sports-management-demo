//
//  SDMCommentCell.h
//  Bloodstock Demo
//
//  Created by Mark Corbyn on 17/03/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDMCommentCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *commentLabel;
@property (weak, nonatomic) IBOutlet UILabel *dateLabel;

@end
