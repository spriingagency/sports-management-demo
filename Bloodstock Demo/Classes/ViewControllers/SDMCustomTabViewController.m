//
//  SDMCustomTabViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 12/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMCustomTabViewController.h"
#import "SDMSettingsViewController.h"

@interface SDMCustomTabViewController () <SDMSettingsViewControllerDelegate, UIPopoverControllerDelegate>
@property (weak, nonatomic) IBOutlet UIView *playerSearchContainerView;
@property (weak, nonatomic) IBOutlet UIView *eventsContainerView;

@property (weak, nonatomic) IBOutlet UIButton *homebutton;
@property (weak, nonatomic) IBOutlet UIButton *eventsButton;
@property (weak, nonatomic) IBOutlet UIButton *settingsButton;
@property (weak, nonatomic) IBOutlet UIButton *notificationsButton;

@property (strong, nonatomic) UIPopoverController *currentPopoverController;
@end

@implementation SDMCustomTabViewController

- (void) viewDidLoad
{
    [super viewDidLoad];
    // Do any additional setup after loading the view.
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    UIViewController *vc = segue.destinationViewController;
    if ([segue isKindOfClass:[UIStoryboardPopoverSegue class]]) {
        
        UIStoryboardPopoverSegue *popoverSegue = (UIStoryboardPopoverSegue*)segue;
        self.currentPopoverController = popoverSegue.popoverController;
        
        if ([vc isKindOfClass:[SDMSettingsViewController class]]) {
            SDMSettingsViewController *settingsVC = (SDMSettingsViewController*)vc;
            settingsVC.delegate = self;
        }
    }
}

#pragma mark - UIPopoverControllerDelegate
-(void) popoverControllerDidDismissPopover:(UIPopoverController *)popoverController
{
    self.currentPopoverController.delegate = nil;
    self.currentPopoverController = nil;
}

#pragma mark - SDMSettingsViewControllerDelegate
-(void) didPressLogout
{
    [self.currentPopoverController dismissPopoverAnimated:NO];
    [self.navigationController popViewControllerAnimated:YES];
}

#pragma mark - Interface Handling
- (IBAction)homeButtonPressed:(UIButton *)sender {
    self.eventsContainerView.hidden = YES;
    self.playerSearchContainerView.hidden = NO;
    
    [self.homebutton setSelected:YES];
    [self.eventsButton setSelected:NO];
}
- (IBAction)eventsButtonPressed:(id)sender {
    self.eventsContainerView.hidden = NO;
    self.playerSearchContainerView.hidden = YES;
    
    [self.homebutton setSelected:NO];
    [self.eventsButton setSelected:YES];
}

@end
