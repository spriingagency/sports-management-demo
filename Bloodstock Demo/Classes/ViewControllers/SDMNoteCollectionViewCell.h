//
//  SDMNoteCollectionViewCell.h
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 13/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface SDMNoteCollectionViewCell : UICollectionViewCell

@property (weak, nonatomic) IBOutlet UILabel *typeLabel;
@property (weak, nonatomic) IBOutlet UILabel *personLabel;
@property (weak, nonatomic) IBOutlet UILabel *dayLabel;
@property (weak, nonatomic) IBOutlet UILabel *monthLabel;
@property (weak, nonatomic) IBOutlet UIImageView *imageView;
@end
