//
//  SDMTransparentSplitViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 17/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMTransparentSplitViewController.h"

@interface SDMTransparentSplitViewController ()

@end

@implementation SDMTransparentSplitViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    self.view.backgroundColor = [UIColor clearColor];
}

@end
