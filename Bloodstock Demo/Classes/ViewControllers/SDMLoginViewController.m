//
//  SDMLoginViewController.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 04/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMLoginViewController.h"
#import "SDMKeyboardReactionManager.h"

@interface SDMLoginViewController ()

@property (nonatomic, weak) IBOutlet NSLayoutConstraint *loginViewToTopConstraint;
@property (nonatomic, weak) IBOutlet UIView *loginFormContainerView;
@property (nonatomic, weak) IBOutlet UITextField *emailTextField;
@property (nonatomic, weak) IBOutlet UITextField *passwordTextField;

@property (nonatomic, strong) SDMKeyboardReactionManager *keyboardManager;

@end

@implementation SDMLoginViewController

-(void) viewDidLoad
{
    [super viewDidLoad];
    
    [self.emailTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:self.emailTextField.placeholder
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor colorWithWhite:1.f alpha:0.5f]}]];
    [self.passwordTextField setAttributedPlaceholder:[[NSAttributedString alloc] initWithString:self.passwordTextField.placeholder
                                                                             attributes:@{NSForegroundColorAttributeName:[UIColor colorWithWhite:1.f alpha:0.5f]}]];
    
    self.keyboardManager = [SDMKeyboardReactionManager new];
}

-(void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self registerKeyboardWatchers];
}

-(void) viewDidDisappear:(BOOL)animated
{
    [super viewDidDisappear:animated];
    [self unregsiterKeyboardWatchers];
}

-(void) prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender
{
    [self.view endEditing:YES];
}

-(void) registerKeyboardWatchers
{
    __weak typeof(self) weakSelf = self;
    
    [self.keyboardManager setKeyBoardWillShowHandler:^(NSDictionary *userInfo) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSNumber *duration = userInfo[@"UIKeyboardAnimationDurationUserInfoKey"];
        CGFloat durationVal = duration.floatValue;
        
        [strongSelf.view layoutIfNeeded];
        [UIView animateWithDuration:durationVal
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             strongSelf.loginViewToTopConstraint.constant = 210;
                             [strongSelf.view layoutIfNeeded];
                         }
                         completion:nil];
        
    }];
    
    [self.keyboardManager setKeyBoardWillHideHandler:^(NSDictionary *userInfo) {
        __strong typeof(weakSelf) strongSelf = weakSelf;
        
        NSNumber *duration = userInfo[@"UIKeyboardAnimationDurationUserInfoKey"];
        CGFloat durationVal = duration.floatValue;
        
        [strongSelf.view layoutIfNeeded];
        [UIView animateWithDuration:durationVal
                              delay:0
                            options:UIViewAnimationOptionBeginFromCurrentState|UIViewAnimationOptionCurveEaseOut
                         animations:^{
                             
                             
                             strongSelf.loginViewToTopConstraint.constant = 0;
                             [strongSelf.view layoutIfNeeded];
                         }
                         completion:nil];
    }];
}

-(void) unregsiterKeyboardWatchers
{
    [self.keyboardManager setKeyBoardWillShowHandler:nil];
    [self.keyboardManager setKeyBoardWillHideHandler:nil];
}

#pragma mark - Unwind
-(IBAction) unwindToLoginScreen:(UIStoryboardSegue*)unwindSegue
{}

@end
