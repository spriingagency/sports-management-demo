//
//  SDMPlayerDataStore.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 18/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMPlayerDataStore.h"

@implementation SDMPlayerDataStore

+(NSArray*) playerNames
{
    return @[
             @[
                 @"A",
                 @"Jose Acasuso",
                 @"David Adams",
                 @"Kimberly	Adams",
                 @"Andre Agassi",
                 @"Ronald Agenor",
                 @"Juan Aguilera",
                 @"Karim Alami",
                 @"Pieter Aldrich",
                 @"Fred Alexander",
                 @"John Alexander",
                 @"Wilmer Alison, Jr.",
                 @"Nicolas Almagro",
                 @"Manuel Alonso",
                 @"Victor Amaya",
                 @"Vijay Amritaj",
                 @"James Anderson",
                 @"Mal Anderson",
                 @"Igor Andreev",
                 @"John Andrews"
                 ],
             @[
                 @"B",
                 @"Charles Bailey",
                 @"Jose Barnes",
                 @"Bobby Baker",
                 @"Louise Bell",
                 @"William Bennett",
                 @"Keith Brooks",
                 @"Nicholas Brown",
                 @"Edward Bryant",
                 @"Carol Butler"
                 ],
             @[
                 @"C",
                 @"Joe Campbell",
                 @"Walter Carter",
                 @"Adam Clark",
                 @"Marie Coleman",
                 @"Henry Collins",
                 @"Randy Cook",
                 @"Andrew Cooper",
                 @"Billy Cox"
                 ],
             @[
                 @"D",
                 @"Russell Diaz"
                 ],
             @[
                 @"E",
                 @"Helen Edwards",
                 @"Sandra Evans"
                 ],
             @[
                 @"F",
                 @"Donna Flores",
                 @"Joyce Foster"
                 ],
             @[
                 @"G",
                 @"Earl	Garcia",
                 @"Tammy Gonzalez",
                 @"Joshua Gray",
                 @"Douglas Green",
                 @"Jennifer	Griffin"
                 ],
             @[
                 @"H",
                 @"Jonathan	Hall",
                 @"Jerry Harris",
                 @"Timothy Henderson",
                 @"Katherine Hill",
                 @"Jeffrey Howard",
                 @"Fred Hughes"
                 ],
             @[
                 @"J",
                 @"Tina	Jackson",
                 @"Rebecca James",
                 @"Harry Johnson",
                 @"Daniel Jones"
                 ],
             @[
                 @"K",
                 @"Alan Kelly",
                 @"Paula King"
                 ],
             @[
                 @"L",
                 @"Andrea Lee",
                 @"Heather Lewis",
                 @"Pamela Long",
                 @"Nicole Lopez"
                 ],
             @[
                 @"M",
                 @"David Martin",
                 @"Eugene Miller",
                 @"Elizabeth Mitchell",
                 @"Sean	Moore",
                 @"Deborah Morgan",
                 @"Ashley Morris",
                 @"Cheryl Murphy"
                 ],
             @[
                 @"N",
                 @"Jeremy Nelson"
                 ],
             @[
                 @"O",
                 @"Nathaniel Osatok"
                 ],
             @[
                 @"P",
                 @"Craig Parker",
                 @"George Perez",
                 @"Marilyn	Perry",
                 @"Raymond Phillips",
                 @"Jean Powell",
                 @"Roger Price"
                 ],
             @[
                 @"R",
                 @"Dennis Ramirez",
                 @"Julia Rivera",
                 @"Jack	Reed",
                 @"Louis Roberts",
                 @"Janice Robinson",
                 @"Virginia	Rodriguez",
                 @"Kevin Rogers",
                 @"Mary	Ross",
                 @"Diana Russell"
                 ],
             @[
                 @"S",
                 @"Denise Sanchez",
                 @"Emily Scott",
                 @"Teresa Simmons",
                 @"James Smith",
                 @"Alice Stewart"
                 ],
             @[
                 @"T",
                 @"Stephen Taylor",
                 @"Kelly Thompson",
                 @"Patricia	Torres",
                 @"Patrick	Turner"
                 ],
             @[
                 @"V",
                 @"John Venbles"
                 ],
             @[
                 @"W",
                 @"Judy	Ward",
                 @"Irene Walker",
                 @"Albert Washington",
                 @"Doris Watson",
                 @"Harold White",
                 @"Angela Wilson",
                 @"Scott Wood",
                 @"Lois	Wright"
                 ],
             @[
                 @"Y",
                 @"Karen Young"
                 ]
             ];
}

@end
