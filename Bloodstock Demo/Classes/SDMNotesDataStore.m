//
//  SDMNotesDataStore.m
//  Bloodstock Demo
//
//  Created by Sean Atkinson on 18/02/2015.
//  Copyright (c) 2015 Spriing. All rights reserved.
//

#import "SDMNotesDataStore.h"

@implementation SDMNotesDataStore

+(NSArray*) notes
{
    return @[
             @{
                 @"name" : @"Olly Jones",
                 @"image" : @"ui_person_one",
                 @"type" : @"TRAINER",
                 @"day" : @"24",
                 @"month" : @"JAN"
                 },
             @{
                 @"name" : @"David Smith",
                 @"image" : @"ui_person_two",
                 @"type" : @"PLAYER",
                 @"day" : @"18",
                 @"month" : @"JAN"
                 },
             @{
                 @"name" : @"Clint Berkley",
                 @"image" : @"ui_person_three",
                 @"type" : @"TRAINER",
                 @"day" : @"12",
                 @"month" : @"JAN"
                 },
             @{
                 @"name" : @"Clint Berkley",
                 @"image" : @"ui_person_three",
                 @"type" : @"TRAINER",
                 @"day" : @"06",
                 @"month" : @"JAN"
                 },
             @{
                 @"name" : @"Olly Jones",
                 @"image" : @"ui_person_one",
                 @"type" : @"TRAINER",
                 @"day" : @"28",
                 @"month" : @"DEC"
                 },
             @{
                 @"name" : @"Clint Berkley",
                 @"image" : @"ui_person_three",
                 @"type" : @"TRAINER",
                 @"day" : @"21",
                 @"month" : @"DEC"
                 },
             @{
                 @"name" : @"Olly Jones",
                 @"image" : @"ui_person_one",
                 @"type" : @"TRAINER",
                 @"day" : @"15",
                 @"month" : @"DEC"
                 },
             @{
                 @"name" : @"Clint Berkley",
                 @"image" : @"ui_person_three",
                 @"type" : @"TRAINER",
                 @"day" : @"30",
                 @"month" : @"NOV"
                 },
             @{
                 @"name" : @"David Smith",
                 @"image" : @"ui_person_two",
                 @"type" : @"PLAYER",
                 @"day" : @"21",
                 @"month" : @"NOV"
                 },
             @{
                 @"name" : @"David Smith",
                 @"image" : @"ui_person_two",
                 @"type" : @"PLAYER",
                 @"day" : @"14",
                 @"month" : @"NOV"
                 },
             @{
                 @"name" : @"Olly Jones",
                 @"image" : @"ui_person_one",
                 @"type" : @"TRAINER",
                 @"day" : @"04",
                 @"month" : @"NOV"
                 },
             ];
}

@end
